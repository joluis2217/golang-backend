package controller

import (
	"crud-cliente/query"
	"crud-cliente/repository"
	"net/http"
)

var (
	clientRepo  = &repository.ClientRepository{}
	clientQuery = &query.ClientQuery{}
)

func GetAll(writer http.ResponseWriter, request *http.Request) {
	clientQuery.GetAll(writer, request)

}
func Get(writer http.ResponseWriter, request *http.Request) {
	clientQuery.Get(writer, request)
}
func GetDNI(writer http.ResponseWriter, request *http.Request) {
	clientQuery.GetDNI(writer, request)
}

func Save(writer http.ResponseWriter, request *http.Request) {
	clientRepo.Save(writer, request)
}

func Update(writer http.ResponseWriter, request *http.Request) {
	clientRepo.Update(writer, request)
}

func Delete(writer http.ResponseWriter, request *http.Request) {
	clientRepo.Delete(writer, request)
}
