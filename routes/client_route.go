package routes

import (
	"crud-cliente/controller"

	"github.com/gorilla/mux"
)

func SetPersonaRoutes(router *mux.Router) {
	subRoute := router.PathPrefix("/api/client").Subrouter()
	subRoute.HandleFunc("/", controller.GetAll).Methods("GET")
	subRoute.HandleFunc("/{id}", controller.Get).Methods("GET")
	subRoute.HandleFunc("/DNI/{dni}", controller.GetDNI).Methods("GET")
	subRoute.HandleFunc("/", controller.Save).Methods("POST")
	subRoute.HandleFunc("/{id}", controller.Delete).Methods("DELETE")
	subRoute.HandleFunc("/{id}", controller.Update).Methods("PUT")
}
