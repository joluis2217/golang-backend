package models

import (
	"fmt"
	"reflect"

	"github.com/go-playground/validator/v10"
)

type Client struct {
	ID        int64  `json:"id" gorm:"primary_key;auto_increment"`
	DNI       string `json:"dni" gorm:"unique_index" validate:"required,len=8" validateDNI:"El campo DNI debe tener exactamente 8 caracteres"`
	Name      string `json:"name" validate:"required" validateName:"El campo Nombre es obligatorio"`
	LastName  string `json:"lastName" gorm:"column:lastname" validate:"required" validateLastName:"El campo Apellido es obligatorio"`
	BirthDate string `json:"birthDate" gorm:"column:birthdate" validate:"required" validateBirthDate:"El campo Fecha de Nacimiento es obligatorio"`
	City      string `json:"city" validate:"required" validateCity:"El campo Ciudad es obligatorio"`
}

var validate *validator.Validate

func init() {
	validate = validator.New()
	validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
		return fld.Tag.Get("validate" + fld.Name)
	})
}
func (c *Client) Validate() error {
	if err := validate.Struct(c); err != nil {
		return err
	}
	return nil
}

func (c *Client) String() string {
	return fmt.Sprintf("ID: %d, DNI: %s, Name: %s, LastName: %s, BirthDate: %s, City: %s", c.ID, c.DNI, c.Name, c.LastName, c.BirthDate, c.City)
}
