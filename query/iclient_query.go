package query

import "net/http"

type IClientQuery interface {
	Get(writer http.ResponseWriter, request *http.Request)
	GetDNI(writer http.ResponseWriter, request *http.Request)
	GetAll(writer http.ResponseWriter, request *http.Request)
}
