package query

import (
	"crud-cliente/commons"
	models "crud-cliente/model"
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type ClientQuery struct {
}

func (c *ClientQuery) Get(writer http.ResponseWriter, request *http.Request) {
	client := models.Client{}

	id := mux.Vars(request)["id"]

	db := commons.GetConnection()
	defer db.Close()

	db.Find(&client, id)

	if client.ID > 0 {
		json, _ := json.Marshal(client)
		commons.SendReponse(writer, http.StatusOK, json)
	} else {
		commons.SendError(writer, http.StatusNotFound)
	}

}

func (c *ClientQuery) GetDNI(writer http.ResponseWriter, request *http.Request) {
	// Obtiene el DNI del parámetro en la URL
	dni := mux.Vars(request)["dni"]

	db := commons.GetConnection()
	defer db.Close()

	client := models.Client{}
	err := db.Where("dni = ?", dni).First(&client).Error

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			commons.SendError(writer, http.StatusNoContent)
		} else {
			log.Fatal(err)
			commons.SendError(writer, http.StatusInternalServerError)
		}
		return
	}

	json, _ := json.Marshal(client)
	commons.SendReponse(writer, http.StatusOK, json)
}

func (c *ClientQuery) GetAll(writer http.ResponseWriter, request *http.Request) {
	clients := []models.Client{}
	db := commons.GetConnection()
	defer db.Close()

	db.Find(&clients)
	json, _ := json.Marshal(clients)
	commons.SendReponse(writer, http.StatusOK, json)
}
