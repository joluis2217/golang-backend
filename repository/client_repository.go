package repository

import (
	"crud-cliente/commons"
	models "crud-cliente/model"
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type ClientRepository struct {
}

func (c *ClientRepository) Save(writer http.ResponseWriter, request *http.Request) {
	client := models.Client{}

	db := commons.GetConnection()
	defer db.Close()

	error := json.NewDecoder(request.Body).Decode(&client)

	if error != nil {
		log.Fatal(error)
		commons.SendError(writer, http.StatusBadRequest)
		return
	}

	if err := client.Validate(); err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}

	error = db.Save(&client).Error

	if error != nil {
		log.Fatal(error)
		commons.SendError(writer, http.StatusInternalServerError)
		return
	}

	json, _ := json.Marshal(client)

	commons.SendReponse(writer, http.StatusCreated, json)
}

func (c *ClientRepository) Update(writer http.ResponseWriter, request *http.Request) {
	clientID := mux.Vars(request)["id"]

	client := models.Client{}
	db := commons.GetConnection()
	defer db.Close()

	if err := db.First(&client, clientID).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			commons.SendError(writer, http.StatusNotFound)
		} else {
			log.Fatal(err)
			commons.SendError(writer, http.StatusInternalServerError)
		}
		return
	}
	if err := json.NewDecoder(request.Body).Decode(&client); err != nil {
		log.Fatal(err)
		commons.SendError(writer, http.StatusBadRequest)
		return
	}

	if err := client.Validate(); err != nil {
		commons.SendError(writer, http.StatusBadRequest)
	}

	if err := db.Save(&client).Error; err != nil {
		log.Fatal(err)
		commons.SendError(writer, http.StatusInternalServerError)
		return
	}

	json, _ := json.Marshal(client)

	commons.SendReponse(writer, http.StatusOK, json)
}

func (c *ClientRepository) Delete(writer http.ResponseWriter, request *http.Request) {
	client := models.Client{}

	db := commons.GetConnection()
	defer db.Close()

	id := mux.Vars(request)["id"]

	db.Find(&client, id)

	if client.ID > 0 {
		db.Delete(client)
		commons.SendReponse(writer, http.StatusOK, []byte(`{}`))
	} else {
		commons.SendError(writer, http.StatusNotFound)
	}
}
